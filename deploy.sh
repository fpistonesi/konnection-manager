#!/bin/bash
 PWD=$(pwd)
 QT="~/Qt/5.11.2/gcc_64"
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QT"/lib/"
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"$PWD/../KonnectionManager/qtermwidget5/"
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"$PWD/../KonnectionManager/lib/"
 export PATH="$QT/bin/":$PATH
../linuxdeployqt "$PWD/KonnectionManager" -verbose=1 -no-copy-copyright-files #-always-overwrite
for f in $(ls "$PWD/lib"); do cp "$QT/lib/$f* "$PWD/lib"; done


#make clean
