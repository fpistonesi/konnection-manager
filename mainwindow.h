#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QDataWidgetMapper>
#include <QMessageBox>
#include <QMdiSubWindow>
#include <QListWidgetItem>
#include <QInputDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QProcess>
#include "qtermwidget.h"
#include "mdisubwindoweventfilter.h"
#include "terminalsettings.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QSqlDatabase Db;
    QSqlTableModel *BookmarksModel;
    QHash<QString, QSqlTableModel*> Models;
    QHash<QString, QDataWidgetMapper*> Mappers;
    QVector<QMdiSubWindow*> Subwindows;
    QVector<QTermWidget*> Terminals;
    QHash<int, QProcess*> Tunnels;

    TerminalSettings *TerminalSettingDlg = new TerminalSettings(this);

    void updateComboGroups();

    void setUpTerminal(QTermWidget *term);

private slots:
    void on_toolBookmarkNew_clicked();

    void on_cmbGroups_currentTextChanged(const QString &arg1);

    void on_toolBookmarkDel_clicked();

    void on_tollBookmarkSave_clicked();

    void on_toolBookmarkRun_clicked();

    void on_actionBookmarks_triggered();

    void on_actionWindows_triggered();

    void on_actionTabs_triggered();


    void on_actionClose_triggered();

    void on_actionRun_triggered();

    void on_listSessions_itemDoubleClicked(QListWidgetItem *item);

    void on_actionSessions_triggered();

    void on_actionFind_triggered();

    void on_listBookmarks_doubleClicked(const QModelIndex &index);

    void on_actionSend_text_triggered();

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

    void on_actionClear_triggered();

    void on_actionLocal_triggered();

    void subwindow_close(QObject *obj, QEvent *e);


    void on_actionRename_triggered();

    void on_actionQuit_triggered();

    void on_actionAboutQt_triggered();

    void on_toolTerminalSettings_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
