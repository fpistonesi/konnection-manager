#ifndef TERMINALSETTINGS_H
#define TERMINALSETTINGS_H

#include <QDialog>
#include "qtermwidget.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QtSql>

namespace Ui {
class TerminalSettings;
}

class TerminalSettings : public QDialog
{
    Q_OBJECT

public:
    explicit TerminalSettings(QWidget *parent = nullptr);
    ~TerminalSettings();

    void init(QSqlRecord record, QSqlTableModel *model, int row);

    QJsonDocument *JsonDocument = new QJsonDocument;
    QSqlRecord Record;
    QSqlTableModel *Model = new QSqlTableModel;
    int ModelIndex;


private slots:
    void on_TerminalSettings_accepted();

private:
    Ui::TerminalSettings *ui;
};

#endif // TERMINALSETTINGS_H
