#ifndef MDISUBWINDOWEVENTFILTER_H
#define MDISUBWINDOWEVENTFILTER_H

#include <QObject>
#include <QMdiSubWindow>
#include <QtEvents>

class MdiSubWindowEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit MdiSubWindowEventFilter(QObject *parent = nullptr);
    bool eventFilter(QObject * obj, QEvent * e);

signals:
    void closeEvent(QObject * obj, QEvent * e);

public slots:
};

#endif // MDISUBWINDOWEVENTFILTER_H
