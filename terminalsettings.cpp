#include "terminalsettings.h"
#include "ui_terminalsettings.h"
#include <QDir>
#include <QtDebug>

TerminalSettings::TerminalSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TerminalSettings)
{
    ui->setupUi(this);
    QTermWidget::addCustomColorSchemeDir(QDir::currentPath()+QDir::separator()+"color-schemes");
    ui->cmbColorScheme->addItems(QTermWidget::availableColorSchemes());
    ui->cmbKeyBindings->addItems(QTermWidget::availableKeyBindings());
}

TerminalSettings::~TerminalSettings()
{
    delete ui;
}

void TerminalSettings::init(QSqlRecord record, QSqlTableModel *model, int row)
{
    this->Record = record;
    this->Model = model;
    this->ModelIndex = row;
        QString jsonString = this->Record.value(this->Record.indexOf("terminal_settings")).toString();
        QJsonParseError *error = new QJsonParseError;
        QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toUtf8(), error);
        this->JsonDocument = &jsonDocument;
        qDebug() << "ERROR" << error->errorString() << "JSON" << jsonString.toUtf8();

        if(this->JsonDocument->object().contains("colorScheme")) {
            ui->cmbColorScheme->setCurrentText(this->JsonDocument->object().value("colorScheme").toString());
        }else{
            ui->cmbColorScheme->setCurrentText("Linux");
        }

        if(this->JsonDocument->object().contains("keyBindings")) {
            ui->cmbKeyBindings->setCurrentText(this->JsonDocument->object().value("keyBindings").toString());
        } else {
            ui->cmbKeyBindings->setCurrentText("default");
        }

        if(this->JsonDocument->object().contains("font")){
            qDebug() << "FONT[R]" << this->JsonDocument->object().value("font");
            QFont font;
            font.fromString(this->JsonDocument->object().value("font").toString());
            ui->fontComboBox->setCurrentFont(font);
            ui->spinFontSize->setValue(font.pointSize());
        }else{
            QFont font;
            font.fromString("Hack,9,-1,5,50,0,0,0,0,0");
            ui->fontComboBox->setCurrentFont(font);
            ui->spinFontSize->setValue(9);
        }

        if(this->JsonDocument->object().contains("shellProgram")) {
            ui->lineShellProgram->setText(this->JsonDocument->object().value("shellProgram").toString());
        } else {
            ui->lineShellProgram->setText("/bin/bash");
        }

        if(this->JsonDocument->object().contains("workingDir")) {
            ui->lineWorkingDir->setText(this->JsonDocument->object().value("workingDir").toString());
        } else {
            ui->lineWorkingDir->setText("");
        }

        if(this->JsonDocument->object().contains("historySize")) {
            ui->spinHistorySize->setValue(this->JsonDocument->object().value("historySize").toInt());
        } else {
            ui->spinHistorySize->setValue(1000);
        }

        if(this->JsonDocument->object().contains("blinkingCursor")) {
            ui->chkBlinkingCursor->setChecked(this->JsonDocument->object().value("blinkingCursor").toBool());
        } else {
            ui->chkBlinkingCursor->setChecked(false);
        }

        if(this->JsonDocument->object().contains("environment")) {
            ui->txtEnvironment->setPlainText(this->JsonDocument->object().value("environment").toString());
        } else {
            ui->txtEnvironment->setPlainText("TERM=xterm-256color\nCOLORTERM=truecolor");
        }
}

void TerminalSettings::on_TerminalSettings_accepted()
{
    QJsonObject JsonObject;

    JsonObject.insert("colorScheme", ui->cmbColorScheme->currentText());
    JsonObject.insert("keyBindings", ui->cmbKeyBindings->currentText());
    QFont font;
    font.fromString(ui->fontComboBox->currentFont().toString());
    font.setPointSize(ui->spinFontSize->value());
    JsonObject.insert("font", font.toString()); qDebug() << "FONT[W]" << JsonObject.value("font");
    JsonObject.insert("shellProgram", ui->lineShellProgram->text());
    JsonObject.insert("workingDir", ui->lineWorkingDir->text());
    JsonObject.insert("historySize", ui->spinHistorySize->value());
    JsonObject.insert("blinkingCursor", ui->chkBlinkingCursor->isChecked());
    JsonObject.insert("environment", ui->txtEnvironment->toPlainText());

    QJsonDocument jsonDocument;
    jsonDocument.setObject(JsonObject);
    this->Record.setValue(this->Record.indexOf("terminal_settings"), jsonDocument.toJson());
    this->Model->setRecord(this->ModelIndex, this->Record);
    //qDebug() << "WRITE" << jsonDocument.toJson();
}
