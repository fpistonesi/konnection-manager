#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->Db = QSqlDatabase::addDatabase("QSQLITE");
    this->Db.setDatabaseName("bookmarks.db");
    this->Db.open();

    QSqlQuery query;
    query.exec("\
        CREATE TABLE bookmark (\
        id                INTEGER PRIMARY KEY AUTOINCREMENT,\
        title             VARCHAR,\
        [group]           VARCHAR,\
        host              VARCHAR,\
        port              INTEGER,\
        username          VARCHAR,\
        password          VARCHAR,\
        options           VARCHAR,\
        cmd               VARCHAR DEFAULT 'ssh',\
        terminal_settings TEXT,\
        is_tunnel         BOOLEAN DEFAULT (0)\
    );\
    ");
//    query.exec("INSERT INTO bookmark(`title`, `group`, `host`, `port`, `username`, `password`) VALUES('Localhost', 'DEV', 'localhost', 3306, 'root', 'passwd')");
//    query.exec("INSERT INTO bookmark(`title`, `group`, `host`, `port`, `username`, `password`) VALUES('Docker', 'DEV', '127.0.0.1', 3306, 'root', 'passwd')");
//    query.exec("INSERT INTO bookmark(`title`, `group`, `host`, `port`, `username`, `password`) VALUES('Server#1', 'PROD', '8.8.8.8', 3306, 'root', 'passwd')");
//    query.exec("INSERT INTO bookmark(`title`, `group`, `host`, `port`, `username`, `password`, `options`, `cmd`) VALUES('TEST', 'ENERRAY', '10.200.4.22', 22, 'rootadmin', 'r00t4dmin', '-oKexAlgorithms=+diffie-hellman-group1-sha1', 'ssh1')");
    qDebug() << query.lastError().text();

    QSqlTableModel *model = new QSqlTableModel;
    model->setTable("bookmark");
    model->setSort(model->record().indexOf("title"),Qt::AscendingOrder);
    model->select();
    this->Models.insert("Bookmark",model);

    QSqlQueryModel *tunnel_model = new QSqlQueryModel;
    tunnel_model->setQuery("\
       SELECT \
       `title` AS `title`,\
       'sshpass -p ' || `password` || ' ' || `cmd` || ' ' || IFNULL(`options`,'') || ' ' || `username` || '@' || `host` || ' -p' || IFNULL(`port`,'22') || ' -L ' || ( 2200 + `id`) || ':%0:%1' AS cmd,\
       ( 2200 + `id`) AS port\
       FROM `bookmark` \
       WHERE `is_tunnel`=true \
       ORDER BY `group`, `title`\
    ");
    qDebug() << tunnel_model->query().lastError().text();
    ui->cmbTunnels->setModel(tunnel_model);
    ui->cmbTunnels->setModelColumn(tunnel_model->record().indexOf("title"));

    ui->listBookmarks->setModel(model);
    ui->listBookmarks->setModelColumn(model->record().indexOf("title"));

    QSqlQueryModel *qModel = new QSqlQueryModel;
    qModel->setQuery("SELECT DISTINCT `group` FROM `bookmark`");
    ui->cmbGroups->setModel(qModel);
    ui->cmbGroup->setModel(qModel);

    QDataWidgetMapper *mapper = new QDataWidgetMapper();
    connect(ui->listBookmarks->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), mapper, SLOT(setCurrentModelIndex(QModelIndex)));
    mapper->setModel(this->Models["Bookmark"]);
    mapper->addMapping(ui->lineTitle, this->Models["Bookmark"]->record().indexOf("title"));
    mapper->addMapping(ui->cmbGroup, this->Models["Bookmark"]->record().indexOf("group"));
    mapper->addMapping(ui->lineHost, this->Models["Bookmark"]->record().indexOf("host"));
    mapper->addMapping(ui->spinPort, this->Models["Bookmark"]->record().indexOf("port"));
    mapper->addMapping(ui->lineUser, this->Models["Bookmark"]->record().indexOf("username"));
    mapper->addMapping(ui->linePassword, this->Models["Bookmark"]->record().indexOf("password"));
    mapper->addMapping(ui->cmbCommand, this->Models["Bookmark"]->record().indexOf("cmd"));
    mapper->addMapping(ui->lineOptions, this->Models["Bookmark"]->record().indexOf("options"));
    mapper->addMapping(ui->chkIsTunnel, this->Models["Bookmark"]->record().indexOf("is_tunnel"));
    mapper->toFirst();
    this->Mappers.insert("Bookmark", mapper);

    this->setCentralWidget(ui->mdiArea);

    ui->scrollArea->hide();
    QStringList themeSearchPaths = QIcon::themeSearchPaths();
    themeSearchPaths << QDir::currentPath()+QDir::separator()+"icons";
    QIcon::setThemeSearchPaths(themeSearchPaths);
    qDebug() << QIcon::themeSearchPaths();
//    QIcon::setThemeName("oxygen");
}

MainWindow::~MainWindow()
{
    delete ui;
    this->Db.close();
}

void MainWindow::updateComboGroups()
{
    QString group = ui->cmbGroups->currentText();
    QSqlQueryModel *qModel = static_cast<QSqlQueryModel*>(ui->cmbGroups->model());
    qDebug() << qModel->query().lastQuery();
    qModel->setQuery(qModel->query().lastQuery());
    ui->cmbGroups->setModel(qModel);
    ui->cmbGroup->setModel(qModel);

    int selectedItem = 0;
    for(int i=0; i<qModel->rowCount(); i++){
        selectedItem = i;
        if(qModel->record(i).value(0).toString() == group) {
            ui->cmbGroups->setCurrentIndex(selectedItem);
            ui->cmbGroup->setCurrentIndex(selectedItem);
            break;
        }
    }

    //

    QString tunnel = ui->cmbGroups->currentText();
    QSqlQueryModel *tModel = static_cast<QSqlQueryModel*>(ui->cmbTunnels->model());
    qDebug() << tModel->query().lastQuery();
    tModel->setQuery(tModel->query().lastQuery());
    ui->cmbTunnels->setModel(tModel);

    int selectedItemT = 0;
    for(int i=0; i<tModel->rowCount(); i++){
        selectedItem = i;
        if(tModel->record(i).value(0).toString() == tunnel) {
            ui->cmbTunnels->setCurrentIndex(selectedItemT);
            break;
        }
    }
}

void MainWindow::setUpTerminal(QTermWidget *term)
{
    QSqlRecord record = this->Models["Bookmark"]->record(this->Mappers["Bookmark"]->currentIndex());
    QString jsonString = record.value(record.indexOf("terminal_settings")).toString();
    QJsonParseError *error = new QJsonParseError;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toUtf8(), error);
    qDebug() << "ERROR" << error->errorString();

    term->addCustomColorSchemeDir(QDir::currentPath()+QDir::separator()+"color-schemes");

    if(jsonDocument.object().contains("colorScheme")) {
        term->setColorScheme(jsonDocument.object().value("colorScheme").toString());
    }

    if(jsonDocument.object().contains("keyBindings")) {
        term->setKeyBindings(jsonDocument.object().value("keyBindings").toString());
    }

    if(jsonDocument.object().contains("font")){
        qDebug() << "FONT[R]" << jsonDocument.object().value("font");
        QFont font;
        font.fromString(jsonDocument.object().value("font").toString());
        term->setTerminalFont(font);
    }

    if(jsonDocument.object().contains("shellProgram")) {
        term->setShellProgram(jsonDocument.object().value("shellProgram").toString());
    }

    if(jsonDocument.object().contains("workingDir")) {
        term->setWorkingDirectory(jsonDocument.object().value("workingDir").toString());
    }

    if(jsonDocument.object().contains("historySize")) {
        term->setHistorySize(jsonDocument.object().value("historySize").toInt());
    }

    if(jsonDocument.object().contains("blinkingCursor")) {
        term->setBlinkingCursor(jsonDocument.object().value("blinkingCursor").toBool());
    }

    if(jsonDocument.object().contains("environment")) {
        term->setEnvironment(jsonDocument.object().value("environment").toString().split("\n"));
        QListIterator<QString> iter(jsonDocument.object().value("environment").toString().split("\n"));
            while (iter.hasNext()) {
                QString pair = iter.next();
                // split on the first '=' character
                int pos = pair.indexOf(QLatin1Char('='));
                if ( pos >= 0 ) {
                    QString variable = pair.left(pos);
                    QString value = pair.mid(pos+1);
                    //setEnv(variable,value);
                    term->sendText("export " + pair + "\n");
                }
            }
            term->clear();
    }else{
        QStringList env;
        env << "TERM=xterm-256color" << "COLORTERM=truecolor";
        term->setEnvironment(env);
    }
}


void MainWindow::on_toolBookmarkNew_clicked()
{
    ui->toolBookmarkEdit->setChecked(true);

    QSqlRecord r = this->Models["Bookmark"]->record();
    r.setValue(this->Models["Bookmark"]->record().indexOf("title"),QString("New #%1").arg(this->Models["Bookmark"]->rowCount()));
    r.setValue(this->Models["Bookmark"]->record().indexOf("group"),ui->cmbGroups->currentText());

    this->Models["Bookmark"]->insertRecord(-1, r);
    this->Models["Bookmark"]->submitAll();
    this->Models["Bookmark"]->select();
    this->Mappers["Bookmark"]->toLast();
    ui->listBookmarks->setCurrentIndex(this->Models["Bookmark"]->index(this->Mappers["Bookmark"]->currentIndex(),1));
}

void MainWindow::on_cmbGroups_currentTextChanged(const QString &arg1)
{
    if(arg1 == ""){
        this->Models["Bookmark"]->setFilter("");
    }else{
        QString filter = QString("`group`='%1'").arg(arg1);
        this->Models["Bookmark"]->setFilter(filter);
    }
}

void MainWindow::on_toolBookmarkDel_clicked()
{
    QMessageBox msg;
    msg.setText(tr("Remove item?"));
    msg.setIcon(QMessageBox::Question);
    msg.setStandardButtons(QMessageBox::Yes|QMessageBox::No);

    if(msg.exec() == QMessageBox::Yes){
        QMessageBox::information(this, tr("Info"), tr("Item deleted"));
        this->Models["Bookmark"]->removeRow(this->Mappers["Bookmark"]->currentIndex());
        this->Mappers["Bookmark"]->toLast();
        ui->listBookmarks->setCurrentIndex(this->Models["Bookmark"]->index(this->Models["Bookmark"]->rowCount()-1,1));

        QSqlQueryModel *qModel = new QSqlQueryModel;
        qModel->setQuery("SELECT DISTINCT `group` FROM `bookmark`");
        ui->cmbGroups->setModel(qModel);
    }
}

void MainWindow::on_tollBookmarkSave_clicked()
{
    this->Models["Bookmark"]->submitAll();
    this->Models["Bookmark"]->select();
    updateComboGroups();
}

void MainWindow::on_toolBookmarkRun_clicked()
{
    qDebug() << "Current Index" << this->Mappers["Bookmark"]->currentIndex();
    if(this->Mappers["Bookmark"]->currentIndex() == -1) return;

    QSqlRecord record = this->Models["Bookmark"]->record(this->Mappers["Bookmark"]->currentIndex());
    int id = record.value(this->Models["Bookmark"]->record().indexOf("id")).toInt();
    QString title = record.value(this->Models["Bookmark"]->record().indexOf("title")).toString();
    QString host = record.value(this->Models["Bookmark"]->record().indexOf("host")).toString();
    QString port = record.value(this->Models["Bookmark"]->record().indexOf("port")).toString();
    QString user = record.value(this->Models["Bookmark"]->record().indexOf("username")).toString();
    QString password= record.value(this->Models["Bookmark"]->record().indexOf("password")).toString();
    QString cmd = record.value(this->Models["Bookmark"]->record().indexOf("cmd")).toString();
    QString options = record.value(this->Models["Bookmark"]->record().indexOf("options")).toString();

    // TUNNEL
    if(ui->chkUseTunnel->isChecked()) {
        QSqlQueryModel *qModel = static_cast<QSqlQueryModel*>(ui->cmbTunnels->model());
        QString tunnel_cmd = qModel->record(ui->cmbTunnels->currentIndex()).value(qModel->record().indexOf("cmd")).toString().arg(host).arg(port);
        qDebug() << tunnel_cmd;
        options.append(" -oStrictHostKeyChecking=no");

        QProcess *proc = new QProcess;
        proc->start(tunnel_cmd);
        proc->waitForStarted();
        proc->write("yes\n");
        proc->waitForReadyRead();
        this->Tunnels.insert(this->Terminals.count()+1, proc);

        host = "127.0.0.1";
        port = qModel->record(ui->cmbTunnels->currentIndex()).value(qModel->record().indexOf("port")).toString();
        title.append(" → [" + ui->cmbTunnels->currentText() + ":"+port+"]");
    }
    //

    // Final Command
    QString command = QString("sshpass -p %1 %2 %3 %4@%5").arg(password).arg(cmd).arg(options).arg(user).arg(host).arg(port);
    if(port != "") command += " -p" + port;
    command += "\n";
    //


    ui->statusBar->showMessage("[" + QTime::currentTime().toString() + "] " + command);

    QMdiSubWindow *win = new QMdiSubWindow;
    QTermWidget *term = new QTermWidget;

    term->setScrollBarPosition(QTermWidget::ScrollBarRight);
    this->setUpTerminal(term);

    term->sendText(command);
    win->setWidget(term);
    win->setWindowTitle(title);
    win->setGeometry(0,0,512,256);
    win->setWindowIcon(QIcon::fromTheme("utilities-terminal"));

    MdiSubWindowEventFilter *ef = new MdiSubWindowEventFilter;
    win->installEventFilter(ef);
    connect(ef, SIGNAL(closeEvent(QObject*, QEvent*)), this, SLOT(subwindow_close(QObject*, QEvent*)));

    ui->mdiArea->addSubWindow(win);
    win->show();

    this->Terminals.append(term);
    this->Subwindows.append(win);

    QListWidgetItem *item = new QListWidgetItem(QIcon::fromTheme("utilities-terminal"), title);
    ui->listSessions->addItem(item);
}

void MainWindow::on_actionBookmarks_triggered()
{
    ui->dockBookmarks->show();
}

void MainWindow::on_actionWindows_triggered()
{
    ui->mdiArea->setViewMode(QMdiArea::SubWindowView);
}

void MainWindow::on_actionTabs_triggered()
{
    ui->mdiArea->setViewMode(QMdiArea::TabbedView);
}

void MainWindow::on_actionClose_triggered()
{
    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Subwindows.remove(i);
            this->Terminals.removeAt(i);
            ui->listSessions->takeItem(i);

            // tunnel process
            if(this->Tunnels.contains(i)) {
                this->Tunnels.value(i)->close();
                this->Tunnels.remove(i);
            }

            break;
        }
    }
    ui->mdiArea->closeActiveSubWindow();
    ui->mdiArea->removeSubWindow(active);
}

void MainWindow::on_actionRun_triggered()
{
    on_toolBookmarkRun_clicked();
}

void MainWindow::on_listSessions_itemDoubleClicked(QListWidgetItem *item)
{
    int i = ui->listSessions->currentRow();
    this->Terminals.at(i)->show();
    this->Subwindows.at(i)->show();
    ui->mdiArea->setActiveSubWindow(this->Subwindows[i]);
}

void MainWindow::on_actionSessions_triggered()
{
    ui->dockSessions->show();
}

void MainWindow::on_actionFind_triggered()
{
    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Terminals.at(i)->toggleShowSearchBar();
            break;
        }
    }
}

void MainWindow::on_listBookmarks_doubleClicked(const QModelIndex &index)
{
    on_actionRun_triggered();
}

void MainWindow::on_actionSend_text_triggered()
{
    QString input = QInputDialog::getMultiLineText(this,tr("Send text"),tr("Send text"));

    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Terminals.at(i)->sendText(input);
            break;
        }
    }
}

void MainWindow::on_actionCopy_triggered()
{
    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Terminals.at(i)->copyClipboard();
            break;
        }
    }
}

void MainWindow::on_actionPaste_triggered()
{
    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Terminals.at(i)->pasteClipboard();
            break;
        }
    }
}

void MainWindow::on_actionClear_triggered()
{
    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Terminals.at(i)->clear();
            break;
        }
    }
}

void MainWindow::on_actionLocal_triggered()
{
    QMdiSubWindow *win = new QMdiSubWindow;
    QTermWidget *term = new QTermWidget;
    QStringList env;
    env << "TERM=xterm-256color" << "COLORTERM=truecolor";
    term->setEnvironment(env);
    term->sendText("export TERM=xterm-256color\nexport COLORTERM=truecolor\n");
    //term->sendText("export TERM=konsole-256color\nexport COLORTERM=truecolor\n");
    //setenv("TERM", "konsole-256color", 1);
    term->changeDir(QDir::homePath());
    term->clear();

    term->addCustomColorSchemeDir(QDir::currentPath()+QDir::separator()+"color-schemes");
    term->setColorScheme("BreezeModified");

    QFile file("/etc/hostname");
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream stream(&file);
    QString title = stream.readAll().trimmed();
    file.close();
    term->setKeyBindings("linux");

    term->setScrollBarPosition(QTermWidget::ScrollBarRight);

    win->setWidget(term);
    win->setWindowTitle(title);
    win->setGeometry(0,0,512,256);
    win->setWindowIcon(QIcon::fromTheme("utilities-terminal"));

    MdiSubWindowEventFilter *ef = new MdiSubWindowEventFilter;
    win->installEventFilter(ef);
    connect(ef, SIGNAL(closeEvent(QObject*, QEvent*)), this, SLOT(subwindow_close(QObject*, QEvent*)));

    ui->mdiArea->addSubWindow(win);
    win->show();

    this->Terminals.append(term);
    this->Subwindows.append(win);

    QListWidgetItem *item = new QListWidgetItem(QIcon::fromTheme("utilities-terminal"), title);
    //item->setFlags(item->flags()|Qt::ItemIsEditable);
    ui->listSessions->addItem(item);
}

void MainWindow::subwindow_close(QObject *obj, QEvent *e)
{
    QMdiSubWindow * active = dynamic_cast<QMdiSubWindow*>(obj);
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            this->Subwindows.remove(i);
            this->Terminals.removeAt(i);
            ui->listSessions->takeItem(i);
            break;
        }
    }
    //ui->mdiArea->closeActiveSubWindow();
    ui->mdiArea->removeSubWindow(active);
    if(ui->mdiArea->viewMode() == QMdiArea::TabbedView && ui->mdiArea->activeSubWindow() != nullptr) {
        ui->mdiArea->activeSubWindow()->showMaximized();
    }
}


void MainWindow::on_actionRename_triggered()
{

    QMdiSubWindow *active =  ui->mdiArea->activeSubWindow();
    for(int i=0; i<this->Subwindows.count(); i++){
        if(this->Subwindows.at(i) == active){
            QString input = QInputDialog::getText(this,tr("Rename"),tr("New window title"),QLineEdit::Normal,this->Subwindows.at(i)->windowTitle());
            if(input != ""){
                this->Subwindows.at(i)->setWindowTitle(input);
                ui->listSessions->item(i)->setText(input);
            }
            break;
        }
    }
}


void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionAboutQt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_toolTerminalSettings_clicked()
{
    QSqlRecord record = this->Models["Bookmark"]->record(this->Mappers["Bookmark"]->currentIndex());
    QSqlTableModel *model = this->Models["Bookmark"];
    int row = this->Mappers["Bookmark"]->currentIndex();
    this->TerminalSettingDlg->init(record, model, row);
    this->TerminalSettingDlg->show();
}
