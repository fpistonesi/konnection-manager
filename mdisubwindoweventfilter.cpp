#include "mdisubwindoweventfilter.h"

MdiSubWindowEventFilter::MdiSubWindowEventFilter(QObject *parent) : QObject(parent)
{

}

bool MdiSubWindowEventFilter::eventFilter(QObject *obj, QEvent *e)
{
    if(e->type() == QEvent::Close){
        emit closeEvent(obj,e);
    }
    return QObject::eventFilter(obj, e);
}
